(function() {
    'use strict';

    angular
        .module('tpAppJavaApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
